$(document).ready(function(argument) {
    var Mydatas = JSON.parse($("#main1").text());
    getData() == null ? putData(Mydatas) : "";
    var k = getData();

    function getData() {
        return JSON.parse(localStorage.getItem("logIn"));
    }

    function putData(data) {
        localStorage.setItem("logIn", JSON.stringify(data));
    }

    // Question Local Storage
    var que = JSON.parse($("#question").text());
    getData1() == null ? putData1(que) : "";
    var kx = getData1();

    function getData1() {
        return JSON.parse(localStorage.getItem("Questions"));
    }

    function putData1(data) {
        localStorage.setItem("Questions", JSON.stringify(data));
    }

    var count = 0;
    $("#user").prop('disabled', true);
    $("#log").prop('disabled', true);
    $("#pQue").prop('disabled', true);


    var $regmail = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    var $regpass = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[~!@#$%^&*()_+-=:;""'<>?/\|'])[0-9a-zA-Z~!@#$%^&*()_+-=:;""'<>?/\|']{8,}$/;
    var users = [];
    var mail = [];
    var pwd = [];
    var keyLower = "qwertyuiopasdfghjklzxcvbnm";
    var keyUpper = "QWERTYUIOPASDFGHJKLZXCVBNM";

    function encrypt(message) {
        var cypherText = "";
        var lowerAlphabet = "abcdefghijklmnopqrstuvwxyz";
        var upperAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var index = null;
        var ch;
        for (var i = 0; i < message.length; i++) {
            ch = message.charAt(i);

            //checks character case whether lowercase or uppercase 
            var isUpper = ch == ch.toUpperCase();
            isUpper ? index = upperAlphabet.indexOf(ch) : index = lowerAlphabet.indexOf(ch);

            //   if it's not a letter, then add it as it is
            if (index == -1) {
                cypherText = cypherText + ch;
            } else {

                // find the corresponding letter in key and add
                isUpper ? cypherText = cypherText + keyUpper.charAt(index) : cypherText = cypherText + keyLower.charAt(index);
            }
        }

        return cypherText;
    };


    function decrypt(cypher) {
        var message = "";
        var lowerAlphabet = "abcdefghijklmnopqrstuvwxyz";
        var upperAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var index = null;
        var ch;

        for (var i = 0; i < cypher.length; i++) {
            ch = cypher.charAt(i);

            //checks character case whether lowercase or uppercase 
            var isUpper = ch == ch.toUpperCase();
            isUpper ? index = keyUpper.indexOf(ch) : index = keyLower.indexOf(ch);

            //   if it's not a letter, then add it as it is
            if (index == -1) {
                message = message + ch;
            } else {

                // find the corresponding letter in key and add
                isUpper ? message = message + upperAlphabet.charAt(index) : message = message + lowerAlphabet.charAt(index);
            }
        }

        return message;
    };



    function exisitingData() {
        for (var i = 0; i < k.logIn.length; i++) {
            users.push(k.logIn[i].name);
            mail.push(k.logIn[i].email);
            pwd.push(k.logIn[i].password);
        }
    }

$( "#dialog" ).dialog({
    autoOpen: false,
      modal: true,
       show: {
        effect: "fade",
        duration: 1000
      },
      hide: {
        effect: "fade",
        duration: 1000
      },
      buttons: {
        LogIn: function() {
           window.location.replace("login.html");
        }
      }
    });


    $("#user").on("click", function(e) {
        exisitingData();
        e.preventDefault();
        if (users.indexOf($("#dname").val()) == -1 && mail.indexOf($("#mail").val()) == -1) {
            var new_user = {
                "name": $("#dname").val(),
                "email": $("#mail").val(),
                "password": encrypt($("#pwd").val())
            };
            k.logIn.push(new_user);
            localStorage.setItem("logIn", JSON.stringify(k));
             $( "#dialog" ).dialog( "open" );

        } else {
            (users.indexOf($("#dname").val()) == -1) ? ($("#msgmail").text("Email already registered"), $("#mail").removeClass("is-valid")) : ($("#msgname").text("User name already exists"),
                $("#dname").removeClass("is-valid"));
        }
    })

    function check(cid, pos) {
        if ($("#" + cid).val() == '') {
            $("#" + pos).text('Field is required');
            $("#" + cid).addClass("is-invalid").removeClass("is-valid")
        }
        $("#SignUp").find(".is-valid").each(function() {
            count++;
        });
        (count == 3) ? ($("#user").prop("disabled", false)) : ($("#user").prop("disabled", true));
        count = 0;
    }

    function Qcheck(cid, pos) {
        if ($("#" + cid).val() == '') {
            $("#" + pos).text('Field is required');
            $("#" + cid).addClass("is-invalid").removeClass("is-valid")
        }
        $("#Qform").find(".is-valid").each(function() {
            count++;
        });
        (count == 3) ? ($("#pQue").prop("disabled", false)) : ($("#pQue").prop("disabled", true));
        count = 0;
    }

    function Lcheck(cid, pos) {
        if ($("#" + cid).val() == '') {
            $("#" + pos).text('Field is required');
            $("#" + cid).addClass("is-invalid").removeClass("is-valid")
        }
    }

    $("#dname").on("input", function() {
        var a = $(this);
        console.log()
        if (a.val().length < 5) {
            $("#msgname").text("Display name must be minimum 5 characters");
            $("#dname").removeClass("is-valid");
        } else if (a.val(a.val().replace(/[0-9_()+_*&^%$#@!~<>?:]/g, ''))) {
            $("#msgname").text("Accepted");
            $("#dname").removeClass("is-invalid").addClass("is-valid");
        }
        check("dname", "msgname")
    });

    $("#mail").on("input", function() {
        (!$(this).val().match($regmail)) ? ($("#msgmail").text("Enter valid mail"), $("#mail").removeClass("is-valid")) : ($("#msgmail").text("Accepted"), $("#mail").removeClass("is-invalid").addClass("is-valid"));
        check("mail", "msgmail")
    });


    $("#pwd").on("input", function() {
        var a = $(this);
        (!a.val().match($regpass)) ? ($("#msgpass").text('Condition: Minimum 8 characters (atleast one LowerCase, UpperCase, Number)'), $("#pwd").removeClass("is-valid")) :
        ($("#msgpass").text("Accepted"), $("#pwd").removeClass("is-invalid").addClass("is-valid"));
        check("pwd", "msgpass")
    });

    $("#Lmail").on("input", function() {
        (!$(this).val().match($regmail)) ? ($("#Lmsgmail").text("Enter valid mail"), $("#Lmail").removeClass("is-valid")) : ($("#Lmsgmail").text(""), $("#Lmail").removeClass("is-invalid").addClass("is-valid"));
        Lcheck("Lmail", "Lmsgmail")
    });

    $("#Lpass").on("input", function() {
        exisitingData();
        if (mail.indexOf($("#Lmail").val()) == -1) {
            $("#Lmsgmail").text("Invalid Email/Not Registered").removeClass("is-valid");
            $("#log").prop("disabled", true)
        } else if ($("#Lpass").val() == decrypt(k.logIn[mail.indexOf($("#Lmail").val())].password)) {
            $("#log").prop("disabled", false)
        } else {
            $("#log").prop("disabled", true)
        }
    });
    $("#redirect").on("click",function(e){
        e.preventDefault();
        window.location.replace("home.html");
    })
    $("#log").on("click", function(e) {
        e.preventDefault();
        localStorage.setItem("SessionIn", k.logIn[mail.indexOf($("#Lmail").val())].name);
        window.location.replace("home.html")
    })
    $("#aQue").on("click",function(e){
        if(localStorage.getItem("SessionIn") === null)
        {
            $(".error1").text("log In required");
        }
        else
        {
            e.preventDefault()
        window.location.replace("askQue.html")
        }
        
    })
    if (localStorage.getItem("SessionIn") === null) {
        console.log(" not logged")
        $("#sessionYes").addClass("d-none");
        $("#sessionNo").removeClass("d-none");
    } else {
        $("#sessionNo").addClass("d-none");
        $("#sessionYes").removeClass("d-none");
    }

    $('.signIn').on('click', function(e) {
        e.preventDefault();
        window.location.replace("login.html");
    })
    $('#index').on('click', function(e) {
        e.preventDefault();
        window.location.replace("index.html");
    })
    $("#qTitle").on("input", function() {
        var a = $(this);

        if (a.val().length > 1) {
            $("#msgTitle").text("Accepted");
            $("#qTitle").addClass("is-valid");
        } else {
            $("#msgTitle").text("Title must be minimum 15 characters");
            $("#qTitle").removeClass("is-valid");
        }
        Qcheck("qTitle", "msgTitle")
    });

    $("#quere").on("input", function() {
        var a = $(this);
        if (a.val().length > 2) {
            $("#msgText").text("Query must be minimum 40 characters");
            $("#quere").addClass("is-valid");
        }
        Qcheck("quere", "msgText")
    });

    $("#qTags").on("input", function() {
        $("#msgTag").text("Seperate tags using ','");
        $("#qTags").addClass("is-valid");
        Qcheck("qTags", "msgTag")
    });

    $("#pQue").on("click", function(e) {
            console.log($("#qTitle").val())
            console.log($("#quere").val())
            console.log($("#qTags").val())

            var qusers = [];
            for (var i = 0; i < kx.questions.length; i++) {
                // console.log(Object.keys(kx.questions[i])[0])
                qusers.push(Object.keys(kx.questions[i])[0])
                    // console.log(qusers)
            }
            if (qusers.indexOf(localStorage.getItem("SessionIn")) != -1) {
                console.log(kx.questions[qusers.indexOf(localStorage.getItem("SessionIn"))][Object.keys(kx.questions[qusers.indexOf(localStorage.getItem("SessionIn"))])[0]].length)
                var new_Query = {
                    "id": kx.questions[qusers.indexOf(localStorage.getItem("SessionIn"))][Object.keys(kx.questions[qusers.indexOf(localStorage.getItem("SessionIn"))])[0]].length,
                    "title": $("#qTitle").val(),
                    "query": $("#quere").val(),
                    "tags": $("#qTags").val(),
                    "comments": []
                }
                kx.questions[qusers.indexOf(localStorage.getItem("SessionIn"))][Object.keys(kx.questions[qusers.indexOf(localStorage.getItem("SessionIn"))])[0]].push(new_Query);
                localStorage.setItem("Questions", JSON.stringify(kx));
                // alert("Succesfully Posted")
        e.preventDefault();
        var av=kx.questions[qusers.indexOf(localStorage.getItem("SessionIn"))][Object.keys(kx.questions[qusers.indexOf(localStorage.getItem("SessionIn"))])[0]].length;
        var an=av-1
        var a=Object.keys(kx.questions[qusers.indexOf(localStorage.getItem("SessionIn"))])[0]+"-"+an;
        var myData = {};
        console.log(a)
        myData["title"] = $("#qTitle").val();
        myData["text"] = $("#quere").val();
        myData["tags"] = $("#qTags").val();
        myData["idd"] = a;
        localStorage.setItem("tryq", JSON.stringify(myData));
        window.location.replace("showQue.html");
            } else {
                var y = {};
                y[localStorage.getItem("SessionIn")] = [];
                kx.questions.push(y);
                var new_Query = {
                    "id": 0,
                    "title": $("#qTitle").val(),
                    "query": $("#quere").val(),
                    "tags": $("#qTags").val(),
                    "comments": []
                }
                y[localStorage.getItem("SessionIn")].push(new_Query);
                localStorage.setItem("Questions", JSON.stringify(kx));
                e.preventDefault();
        var a=Object.keys(kx.questions[qusers.indexOf(localStorage.getItem("SessionIn"))])[0]+"-"+0;
        var myData = {};
        myData["title"] = $("#qTitle").val();
        myData["text"] = $("#quere").val();
        myData["tags"] = $("#qTags").val();
        myData["idd"] = a;
        localStorage.setItem("tryq", JSON.stringify(myData));
        window.location.replace("showQue.html");
            }


        })
    for (var a = 0; a < kx.questions.length; a++) {
        for (var b = 0; b < kx.questions[a][Object.keys(kx.questions[a])[0]].length; b++) {

            $("#topQue").append('<div class="col-md-4 col-sm-4 loadQue" id="' + Object.keys(kx.questions[a])[0] + '-' + kx.questions[a][Object.keys(kx.questions[a])[0]][b].id + '" >' +
                '<div class="card border-success mb-3" style="max-width: 18rem;">' +
                '<div class="card-header">' + "Tags: " + kx.questions[a][Object.keys(kx.questions[a])[0]][b].tags + '</div>' +
                '<div class="card-body text-success">' +
                '<h5 class="card-title text-truncate">' + kx.questions[a][Object.keys(kx.questions[a])[0]][b].title + '</h5>' +
                '<p class="card-text text-body text-truncate">' + kx.questions[a][Object.keys(kx.questions[a])[0]][b].query + '</p>' +
                '</div></div></div>')
        }
    }


    $(".loadQue").on("click", function(e) {
        e.preventDefault();
        var myData = {};
        myData["title"] = $(this).find(".card-title").text();
        myData["text"] = $(this).find(".card-text").text();
        myData["tags"] = $(this).find(".card-header").text();
        myData["idd"] = $(this).attr('id');
        localStorage.setItem("tryq", JSON.stringify(myData));
        window.location.replace("showQue.html");
    })

    $("#sessionYes").on("click",function(){
        localStorage.removeItem("SessionIn");
    })
});